﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_Task2_Drop_DownList
{
    public partial class Default : System.Web.UI.Page
    {
        protected async void Page_Load(object sender, EventArgs e)
        {
            if(IsPostBack==false)
            {
                await this.ColorDropDownListBind();
                ddlColors.DataBind();

                ddlColors.AppendDataBoundItems = true;
                ddlColors.Items.Insert(0, new ListItem("-- Select City --", "0"));
                ddlColors.SelectedIndex = 0;

            }
        }

        #region Private Methods
        private async Task ColorDropDownListBind()
        {
            await Task.Run(() => {

                ddlColors.Items.Add(new ListItem("Red", "1"));
                ddlColors.Items.Add(new ListItem("Green", "2"));
                ddlColors.Items.Add(new ListItem("Yellow", "3"));

            });
        }
        #endregion

        protected void ddlColors_SelectedIndexChanged(object sender, EventArgs e)
        {

            if(ddlColors.SelectedValue=="1")
            {
                divContent1.Style["background-color"] = "red";
            }
            else if(ddlColors.SelectedValue=="2")
            {
                divContent1.Style["background-color"] = "green";
            }
            else if(ddlColors.SelectedValue=="3")
            {
                divContent1.Style["background-color"] = "yellow";
            }

        }
    }
}